# -*- coding: utf-8 -*-
"""
Spyder Editor

Author: Eugen Ruff
email: eugen.ruff@tuebingen.mpg.de
"""


try:
  import xml.etree.ElementTree as et
  print("running with lxml.etree")
except ImportError:
    print ImportError.message()
    exit(1)
try:
  import os
except ImportError:
    print ImportError.message()
    exit(1)
try:
    import re # regex
except ImportError:
    print ImportError.message()
    exit(1)
import sys    
import pydot
'''
    findXML 
        findXML searches the given direcotry recursivly
        for xml files but leaves out .svn .git and branch folder
        param
            path: path from where it starts 
        return
            xmlFiles: list of xmlFiles which qualified
'''
def findXML(path):
    xmlFiles = [] # init xmlFiles list
    # using os walk to recusivly search direktories    
    
    for dirname, dirnames, filenames in os.walk(path):
        # loop over filenames
        for filename in filenames:
            # filter filenames for xml files
            
            if os.path.splitext(filename)[0] == "DONE":
               completedPackages.append(os.path.split(dirname)[1])
            if os.path.splitext(filename)[1] == ".xml":
               xmlFiles.append(os.path.join(dirname, filename))
           
        # editing the 'dirnames' list will stop os.walk() from recursing into there.
        if '.git' in dirnames:
            # don't go into any .git directories.
            dirnames.remove('.git')
        # don't traverse the branch folder
        if 'branch' in dirnames:
            dirnames.remove('branch')      
        # don't look into svn subfolder 
        if '.svn' in dirnames:
            dirnames.remove('.svn')        
    # some debug output
    return xmlFiles
'''
    cleanXMLfiles
        go over a list of xml files to filter for manifest package and stack xml files
    param
        inputFiles: list of xml files
    return
        xmlFiles: list of xml files containing only mainifest.xml package.xml and stack.xml
'''
def cleanXMLfiles(inputFiles):
    print 'looking for package.xml'
    xmlFiles = []
    # thats a bit of a hack for I don't see why it doesn't remove all the 
    # wrong files at once, so I check the length of the removed files 
    removedFiles = []
    # insert dummy for the first iteration
    removedFiles.append(['dummy'])
    
    xmlFiles = list(inputFiles)
    # as long as we remove files we look for more
    while len(removedFiles) > 0:
        removedFiles = []
        for xmlFile in xmlFiles:  # loop over the xmlFiles
        # filter for manifest package and stack xml files
            if(os.path.split(xmlFile)[1] != 'package.xml'):
                xmlFiles.remove(xmlFile) # remove file from list
                removedFiles.append(xmlFile) # append removed file to check if any files have been removed
   
    # return the cleaned list of xml files we want to examin further
    return xmlFiles


def findBuildDependencies(root,package_name):

    for xmlNode in root.findall('build_depend'):
        
        if not graph.get_node(xmlNode.text):
            if compareOldNewNaming(xmlNode.text):
                xmlNode.text = compareOldNewNaming(xmlNode.text)
            else:
                graph.add_node(pydot.Node(xmlNode.text))
        edge = pydot.Edge(package_name,xmlNode.text)
        
        graph.add_edge(edge)
    
    outdegree = len(root.findall('build_depend')) - \
        len([xmlNode for xmlNode in root.findall("build_depend")  \
            if ''.join([i for i in xmlNode.text if not i.isdigit()]) not in completedPackages])
         
    outdegrees.append(( outdegree,package_name))
            
def compareOldNewNaming(nodeName):
    nodes = graph.get_nodes()
    for item in nodes:
        if ''.join([i for i in item.get_name() if not i.isdigit()])  == nodeName:
            return item.get_name()
            
def findNodesWithOldNames(graph):
    for item in graph.get_nodes():
        for item2 in graph.get_nodes():
            if ''.join([i for i in item2.get_name() if not i.isdigit()]) == item.get_name():
                if item.get_name() not in item2.get_name():
                    mergeNodes(item.get_name(),item2.get_name(),graph)
    return graph
                        
def mergeNodes(nodeOld,nodeNew,graph):
    
    pyNode_del = graph.get_node(nodeOld)[0]
    pyNode = graph.get_node(nodeNew)[0]
    for edge in graph.get_edges():
        if edge.get_destination() == pyNode_del.get_name():
            newEdge = pydot.Edge(edge.get_source(),pyNode.get_name())
            graph.add_edge(newEdge)
            graph.del_edge(edge.get_source(),edge.get_destination())
        if edge.get_source() == pyNode_del.get_name():
            newEdge = pydot.Edge(pyNode.get_name(),edge.get_destination())
            graph.add_edge(newEdge)
            graph.del_edge(edge.get_source(),edge.get_destination())
            edge.get_source(),edge.get_destination()
            
    
                
    if pyNode_del.get_fillcolor():
        pyNode.set_fillcolor(pyNode_del.get_fillcolor())
        pyNode.set_style('filled')            
    if pyNode_del.get_shape():
        pyNode.set_shape(pyNode_del.get_shape())        
    
    # print pyNode_del.get_name(),
    graph.del_node(pyNode_del.get_name())
    
    return graph
    

                   
def findPackageName(root):
    for xmlNode in root.findall('name'):
        pyNode = pydot.Node(xmlNode.text,shape='box')
        if ''.join([i for i in xmlNode.text if not i.isdigit()]) in completedPackages:
            saturation = 1.00
            value = 1.00
            hue = 0.3
            hsv = "{0:.2f}".format(hue) + " " + "{0:.2f}".format(saturation)+ " " + "{0:.2f}".format(value)
            pyNode.set_fillcolor(hsv)    
            pyNode.set_style('filled')
            pyNode.set_penwidth('2.5')

        graph.add_node(pyNode)
        return xmlNode.text


completedPackages = []
# open the current folder         
allXMLFiles = findXML(sys.argv[1])
# remove all non mainifest stack and package xml files from the list
cleanedXMLFiles = list(cleanXMLfiles(allXMLFiles))

graph = pydot.Dot(graph_type='digraph',rankdir="LR",
                  repulsiveforce='2.2', compound='true',forcelabels='true')
graph.create()
outdegrees = []
for xmlFile in cleanedXMLFiles:
    # get the root of the xml tree
    eTree = et.parse(xmlFile)
    package_name = findPackageName(eTree.getroot())
    
    findBuildDependencies(eTree.getroot(),package_name)


'''
    represent the outdegree as color
'''
min_ = min(i[0] for i in outdegrees)
max_ = max(i[0] for i in outdegrees)

for item in outdegrees:   
    
    if ''.join([i for i in item[1] if not i.isdigit()]) not in completedPackages:
        # define color offset
        color_min = 300
        # determine the linear cost function 
        a = (1000-color_min)/max((max_-min_),1)
        # calculate hue
        hue = (a*item[0]+color_min)/1000.0
        saturation = 1.00
        value = 1.00
        hsv = "{0:.2f}".format(hue) + " " + "{0:.2f}".format(saturation)+ " " + "{0:.2f}".format(value)
        pyNode = graph.get_node(item[1])[0]
        pyNode.set_fillcolor(hsv)
        pyNode.set_style('filled')



graph = findNodesWithOldNames(graph)

        
    
outfile = os.path.splitext(sys.argv[2])[0]
if not outfile:
    raise SystemExit
outputFormat = "pdf"
try: graph.write(outfile + '.'+str(outputFormat),format=str(outputFormat))
except pydot.InvocationException as error:
    print error